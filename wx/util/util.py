from random import choice
import subprocess


# opts_example = {'A': 1, 'B': 2, 'C': 3}
def weight_choice(opts):
    temp_list = []
    for opt, weight in opts.items():
        for i in range(weight):
            temp_list.append(opt)
    return choice(temp_list)


def is_num(c):
    if c == '1' or c == '2' or c == '3' or c == '4 ' or c == '5' \
            or c == '6' or c == '7' or c == '8' or c == '9' or c == '0':
        return True
    return False


def fortune():
    p = subprocess.Popen('/usr/games/fortune', shell=True, stdout=subprocess.PIPE)
    return p.stdout.read()
