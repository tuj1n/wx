from sanic.response import text
from xml.etree import ElementTree
from hashlib import sha1
from time import time
from wx import app
from wx.controller.parse import deal_input
from wx.controller.status import user_status


@app.route('/', methods=['GET', 'POST'])
async def auth(request):
    if request.method == 'GET':
        token = 'wx2017'
        data = request.args
        signature = data.get('signature', '')
        timestamp = data.get('timestamp', '')
        nonce = data.get('nonce', '')
        echostr = data.get('echostr', '')
        s = [token, timestamp, nonce]
        s.sort()
        s = ''.join(s)

        if sha1(s.encode('utf-8')).hexdigest() == signature:
            return text(echostr)

        return text('')
    else:
        xml_data = ElementTree.fromstring(request.body)
        to_user_name = xml_data.find("ToUserName").text
        from_user_name = xml_data.find("FromUserName").text
        user_status.setdefault(from_user_name, 0)
        content = xml_data.find("Content").text
        print('content: ' + content)
        reply = "<xml><ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]>" \
                "</FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[text]]></MsgType>" \
                "<Content><![CDATA[%s]]></Content><FuncFlag>0</FuncFlag></xml>"
        res = reply % (from_user_name, to_user_name, str(int(time())), deal_input(content, from_user_name))
        return text(res, content_type='application/xml')

