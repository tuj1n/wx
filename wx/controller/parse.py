from wx.util.util import is_num, weight_choice, fortune
from wx.controller.status import user_status

STATUS = {
    'normal': 0,
    'choice': 1,
    'test': 2
}


def deal_input(content, from_user_name):

    if content == 'fortune':
        if user_status[from_user_name] == STATUS['normal']:
            return fortune()

    if content == '选择':
        if user_status[from_user_name] == STATUS['normal']:
            user_status[from_user_name] = STATUS['choice']
            return '请输入选项及权重，如：火锅1 烧烤2 中餐5，不输入权重则默认为1，回复【取消】返回正常模式'

    if content == 'test':
        if user_status[from_user_name] == STATUS['normal']:
            user_status[from_user_name] = STATUS['test']
            return '开发中，回复【取消】返回正常模式'

    if content == '取消':
        if user_status[from_user_name] != STATUS['normal']:
            user_status[from_user_name] = STATUS['normal']
            return '已恢复正常模式'

    else:
        if user_status[from_user_name] == STATUS['choice']:
            opts = {}
            for opt in content.split():
                if opt.isdigit():
                    return '答应我选项不要全都输数字好吗'

                has_weight = False
                for i in range(len(opt)):
                    if is_num(opt[i]):
                        has_weight = True
                        try:
                            opts[opt[:i]] = int(opt[i:])
                        except ValueError:
                            return '请输入正确的选项'

                if not has_weight:
                    opts[opt] = 1

            return weight_choice(opts)

        if user_status[from_user_name] == STATUS['test']:
            return '开发中，回复【取消】返回正常模式'

        return '回复【选择】解决选择困难症\n回复【test】进入测试模式\n回复 【fortune】试试？'
